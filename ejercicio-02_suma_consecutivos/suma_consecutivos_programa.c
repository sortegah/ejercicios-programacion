#include<stdio.h>

main ()
{
	int num, suma, i = 1;
	
	printf ("Programa que recibe un numero entre 1 y 50 y devuelve\n"
     "la suma de los consecutivos de 1 hasta el numero ingresado\n");
    
    do{
	
       printf ("Ingrese un numero entre 1 y 50: ");
       scanf ("%d", &num);
       if (num<1 || num>50)
           printf ("El numero que ingreso no se encuentra dentro del rago.");
	}while (num<1 || num>50);
	
	for (i=1; i<=num; i++){
		suma = suma+ i;
	}
	printf ("La suma de los consecutivos es: %d\n", suma);
}


