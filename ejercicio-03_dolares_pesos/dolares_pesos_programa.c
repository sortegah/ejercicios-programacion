#include<stdio.h>

/*
	Name: dolares_pesos
	Copyright: 
	Author: Sandra Ortega Hernandez
	Date: 18/08/20 00:54
	Description: Elabora un programa que reciba como entrada un monto en d�lares 
				(USD) y devuelva la cantidad equivalente en pesos mexicanos (MXN) 
*/


main()
{
	float conversionPesos, montoDolares, dolar = 21.90;
	
	printf ("Programa que recibe un monto en dolares y devuelve la cantidad equivalente en pesos mexicanos\n");
	printf ("Ingrese el monto a convertir: ");
	scanf ("%f", &montoDolares);
	conversionPesos = (montoDolares * dolar);
	printf ("El equivalente en pesos mexicanos es: %.2f", conversionPesos);
}
