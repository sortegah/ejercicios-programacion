#include<stdio.h>

/*
	Name: isosceles
	Copyright: 
	Author: Sandra Ortega Hernandez
	Date: 18/08/20 02:28
	Description: Calcula el perímetro de un triángulo isósceles
*/

main ()
{
	float lado_a, lado_b, perimetro;
	
	printf ("Este progrmam calcula el perimetro de un triangulo isosceles.\n");
	
	do{
		printf ("Ingrese el tamanio del lado igual del triangulo: ");
		scanf ("%f", &lado_a);
		printf ("Ingrese el tamanio del lado diferente del triangulo: ");
		scanf ("%f", &lado_b);
		if (lado_a<=0 || lado_b<=0)
			printf ("Error, no se acepta el 0 y valores negativos. Por favor, vuelve a ingresar otro valor.\n");
	}while (lado_a<=0 || lado_b<=0);
	
	perimetro = (lado_a * 2) + lado_b;
	printf ("El perimetro del triangulo isosceles es: %.2f", perimetro);
}
