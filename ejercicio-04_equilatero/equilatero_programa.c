#include<stdio.h>

/*
	Name: equilatero
	Copyright: 
	Author: Sandra Ortega Hernandez
	Date: 18/08/20 01:43
	Description: Calcula el per�metro de un tri�ngulo equil�tero
*/

main ()
{
	float lado, perimetro;
	
	printf ("Este programa calcula el perimetro de un triangulo equilatero\n");
	do{
		printf ("Ingrese el tamanio de un lado del triangulo: ");
		scanf ("%f", &lado);
		if (lado<=0)
			printf ("Error, no se acepta el 0 y valores negativos. Por favor, vuelve a ingresar otro valor.\n");
			
	}while (lado<=0);
	
	perimetro = lado * 3;
	printf ("El perimetro del triangulo equilatero es: %.2f", perimetro);
	
}
