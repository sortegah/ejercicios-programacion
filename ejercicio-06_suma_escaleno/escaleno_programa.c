#include<stdio.h>

/*
	Name: Escaleno
	Copyright: 
	Author: Sandra Ortega Hernandez
	Date: 18/08/20 09:23
	Description: Calcula el perímetro de un triángulo escaleno
*/

main ()
{
	float lado_a, lado_b, lado_c, perimetro;
	
	printf ("Este programa calcula el perimetro de un triangulo escaleno.\n");
	
	do
	{
		printf ("Ingrese el primer lado: ");
		scanf ("%f", &lado_a);
		printf ("Ingrese el segundo lado: ");
		scanf ("%f", &lado_b);
		printf ("Ingrese el tercer lado: ");
		scanf ("%f", &lado_c);
		if (lado_a<=0 || lado_b<=0 || lado_c<=0)
			printf ("Error, no se acepta el 0 y valores negativos. Por favor, vuelve a ingresar otro valor.\n");
	}while (lado_a<=0 || lado_b<=0 || lado_c<=0);
	
	perimetro = lado_a + lado_b + lado_c;
	printf ("El perimetro del triangulo escaleno es: %.2f", perimetro);
}
